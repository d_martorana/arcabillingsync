﻿namespace ArcaBillingSync
{
    partial class frmMain
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.myTrayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.txtFtpHost = new System.Windows.Forms.TextBox();
            this.lstFile = new System.Windows.Forms.ListBox();
            this.txtCompatta = new System.Windows.Forms.Button();
            this.grpDownload = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCartellaLocale = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lblFtpUltimaAzione = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblDownload = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.grpUpload = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblUltimoInviato = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtZipFolder = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lblUltimoZip = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSftpHost = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.grpDownload.SuspendLayout();
            this.grpUpload.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 277);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(528, 29);
            this.button1.TabIndex = 0;
            this.button1.Text = "Verifica e Download ora";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(546, 351);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(125, 47);
            this.button2.TabIndex = 1;
            this.button2.Text = "SFTP Connect";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // myTrayIcon
            // 
            this.myTrayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("myTrayIcon.Icon")));
            this.myTrayIcon.Text = "notifyIcon1";
            this.myTrayIcon.Visible = true;
            this.myTrayIcon.DoubleClick += new System.EventHandler(this.myTrayIcon_DoubleClick);
            // 
            // txtFtpHost
            // 
            this.txtFtpHost.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFtpHost.Enabled = false;
            this.txtFtpHost.Location = new System.Drawing.Point(99, 19);
            this.txtFtpHost.Name = "txtFtpHost";
            this.txtFtpHost.Size = new System.Drawing.Size(423, 20);
            this.txtFtpHost.TabIndex = 9;
            // 
            // lstFile
            // 
            this.lstFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstFile.FormattingEnabled = true;
            this.lstFile.Location = new System.Drawing.Point(10, 101);
            this.lstFile.Name = "lstFile";
            this.lstFile.Size = new System.Drawing.Size(512, 121);
            this.lstFile.TabIndex = 13;
            // 
            // txtCompatta
            // 
            this.txtCompatta.Location = new System.Drawing.Point(12, 441);
            this.txtCompatta.Name = "txtCompatta";
            this.txtCompatta.Size = new System.Drawing.Size(528, 24);
            this.txtCompatta.TabIndex = 14;
            this.txtCompatta.Text = "Compatta ora i file da inviare";
            this.txtCompatta.UseVisualStyleBackColor = true;
            this.txtCompatta.Click += new System.EventHandler(this.txtCompatta_Click);
            // 
            // grpDownload
            // 
            this.grpDownload.Controls.Add(this.label1);
            this.grpDownload.Controls.Add(this.txtCartellaLocale);
            this.grpDownload.Controls.Add(this.label7);
            this.grpDownload.Controls.Add(this.lblFtpUltimaAzione);
            this.grpDownload.Controls.Add(this.label6);
            this.grpDownload.Controls.Add(this.lblDownload);
            this.grpDownload.Controls.Add(this.lstFile);
            this.grpDownload.Controls.Add(this.label5);
            this.grpDownload.Controls.Add(this.txtFtpHost);
            this.grpDownload.Location = new System.Drawing.Point(12, 12);
            this.grpDownload.Name = "grpDownload";
            this.grpDownload.Size = new System.Drawing.Size(528, 259);
            this.grpDownload.TabIndex = 15;
            this.grpDownload.TabStop = false;
            this.grpDownload.Text = "Verifica presenza e download dei file dal server FTP";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Cartella Locale:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCartellaLocale
            // 
            this.txtCartellaLocale.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCartellaLocale.Enabled = false;
            this.txtCartellaLocale.Location = new System.Drawing.Point(99, 45);
            this.txtCartellaLocale.Name = "txtCartellaLocale";
            this.txtCartellaLocale.Size = new System.Drawing.Size(423, 20);
            this.txtCartellaLocale.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 234);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Ultima Azione:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFtpUltimaAzione
            // 
            this.lblFtpUltimaAzione.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFtpUltimaAzione.Location = new System.Drawing.Point(99, 234);
            this.lblFtpUltimaAzione.Name = "lblFtpUltimaAzione";
            this.lblFtpUltimaAzione.Size = new System.Drawing.Size(423, 13);
            this.lblFtpUltimaAzione.TabIndex = 15;
            this.lblFtpUltimaAzione.Text = "Attesa";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Azione corrente:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDownload
            // 
            this.lblDownload.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDownload.Location = new System.Drawing.Point(99, 85);
            this.lblDownload.Name = "lblDownload";
            this.lblDownload.Size = new System.Drawing.Size(423, 13);
            this.lblDownload.TabIndex = 13;
            this.lblDownload.Text = "Attesa";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "FTP Host Name:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // grpUpload
            // 
            this.grpUpload.Controls.Add(this.label8);
            this.grpUpload.Controls.Add(this.lblUltimoInviato);
            this.grpUpload.Controls.Add(this.label9);
            this.grpUpload.Controls.Add(this.txtZipFolder);
            this.grpUpload.Controls.Add(this.label10);
            this.grpUpload.Controls.Add(this.lblUltimoZip);
            this.grpUpload.Controls.Add(this.label12);
            this.grpUpload.Controls.Add(this.txtSftpHost);
            this.grpUpload.Location = new System.Drawing.Point(12, 312);
            this.grpUpload.Name = "grpUpload";
            this.grpUpload.Size = new System.Drawing.Size(528, 123);
            this.grpUpload.TabIndex = 16;
            this.grpUpload.TabStop = false;
            this.grpUpload.Text = "Compatta ed Invia";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "Ultimo file inviato";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblUltimoInviato
            // 
            this.lblUltimoInviato.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUltimoInviato.Location = new System.Drawing.Point(103, 97);
            this.lblUltimoInviato.Name = "lblUltimoInviato";
            this.lblUltimoInviato.Size = new System.Drawing.Size(419, 13);
            this.lblUltimoInviato.TabIndex = 26;
            this.lblUltimoInviato.Text = "Nessuno";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1, 51);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "Cartella file Zippati:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtZipFolder
            // 
            this.txtZipFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtZipFolder.Enabled = false;
            this.txtZipFolder.Location = new System.Drawing.Point(103, 48);
            this.txtZipFolder.Name = "txtZipFolder";
            this.txtZipFolder.Size = new System.Drawing.Size(419, 20);
            this.txtZipFolder.TabIndex = 24;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 75);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Ultimo file creato:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblUltimoZip
            // 
            this.lblUltimoZip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUltimoZip.Location = new System.Drawing.Point(103, 75);
            this.lblUltimoZip.Name = "lblUltimoZip";
            this.lblUltimoZip.Size = new System.Drawing.Size(419, 13);
            this.lblUltimoZip.TabIndex = 19;
            this.lblUltimoZip.Text = "Nessuno";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "SFTPHost Name:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSftpHost
            // 
            this.txtSftpHost.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSftpHost.Enabled = false;
            this.txtSftpHost.Location = new System.Drawing.Point(103, 19);
            this.txtSftpHost.Name = "txtSftpHost";
            this.txtSftpHost.Size = new System.Drawing.Size(419, 20);
            this.txtSftpHost.TabIndex = 17;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(41, 481);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(166, 30);
            this.button3.TabIndex = 17;
            this.button3.Text = "Ferma Parti Esci";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 519);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.grpUpload);
            this.Controls.Add(this.grpDownload);
            this.Controls.Add(this.txtCompatta);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.grpDownload.ResumeLayout(false);
            this.grpDownload.PerformLayout();
            this.grpUpload.ResumeLayout(false);
            this.grpUpload.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.NotifyIcon myTrayIcon;
        private System.Windows.Forms.TextBox txtFtpHost;
        private System.Windows.Forms.ListBox lstFile;
        private System.Windows.Forms.Button txtCompatta;
        private System.Windows.Forms.GroupBox grpDownload;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblDownload;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox grpUpload;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblFtpUltimaAzione;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblUltimoZip;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSftpHost;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtZipFolder;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblUltimoInviato;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCartellaLocale;
        private System.Windows.Forms.Button button3;
    }
}

