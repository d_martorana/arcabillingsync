﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using CM18tool;
using System.IO.Compression;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using WinSCP;
//using System.Resources;

    /*  [ftpCredentials]
        host=ftp://172.22.0.164
        user=d.martorana
        password=Zidane4
    */

namespace ArcaBillingSync
{

   
    public partial class frmMain : Form
    {
        Timer timerDownload = new Timer();
        Timer timerUpload = new Timer();
        IniFile myIniConfig = new IniFile(Application.StartupPath + "\\config.ini");
        string dwlFtpFolder, dwlLocalFolder;
        string uplLocalFolder, upLocalZipFolder, uplLocalZipSended, uplSftpFolder,uplLocalZipToSend;
        string ftpHost, ftpUser, ftpPassword, sftpHost, sftpUser, sftpPassword;

        public static int sftpConnection()
        {
            try
            {
                SessionOptions sessionOprtions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = "172.22.0.164",
                    UserName = "ARCAIT/d.martorana",
                    Password = "Zidane4",
                    SshHostKeyFingerprint= "ssh-rsa 2048 3b:f4:96:dc:76:20:55:83:5c:8b:51:6f:50:54:59:48"
                };

                using (Session session = new Session())
                {
                    //connect
                    session.Open(sessionOprtions);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return 0;
        }

        public frmMain()
        {
            InitializeComponent();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SpostaFileDaFTP();
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            myTrayIcon.Visible = false;
        }

        void SpostaFileDaFTP()
        {
            Application.DoEvents();
            grpDownload.BackColor = Color.LightSalmon;
            grpDownload.Refresh();
            lstFile.Items.Clear();
            List<string> nomeFile = new List<string>();
            try
            {
                lblDownload.Text = "Cerco nuovi file";
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpHost);
                request.Credentials = new NetworkCredential(ftpUser, ftpPassword);
                request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();

                StreamReader reader = new StreamReader(responseStream);
                while (reader.EndOfStream == false)
                {
                    nomeFile.Add(reader.ReadLine().Substring(39));
                }
                reader.Close();
                response.Close();
                grpDownload.Refresh();
                for (int i = 0; i < nomeFile.Count(); i++)
                {
                    lstFile.Items.Add(nomeFile[i]);
                }

                lblDownload.Text = "Copio i file in elenco";
                for (int i = 0; i < nomeFile.Count; i++)
                {
                    //copia file
                    if (nomeFile[i].Substring(nomeFile[i].Length - 3).ToUpper() == "XML")
                    {
                        try
                        {
                            this.Text = "Copio " + lstFile.Items[i];
                            request = (FtpWebRequest)WebRequest.Create(ftpHost + "/" + lstFile.Items[i]);
                            request.Credentials = new NetworkCredential(ftpUser, ftpPassword);
                            request.Method = WebRequestMethods.Ftp.DownloadFile;
                            response = (FtpWebResponse)request.GetResponse();
                            responseStream = response.GetResponseStream();
                            reader = new StreamReader(responseStream);
                            lblDownload.Text= dwlLocalFolder + "/" + lstFile.Items[i];
                            StreamWriter writer = new StreamWriter(dwlLocalFolder + "/" + lstFile.Items[i]);
                            writer.Write(reader.ReadToEnd());
                            writer.Close();
                            reader.Close();
                            response.Close();
                            this.Text = "Ho copiato " + lstFile.Items[i];
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(lstFile.Items[i] + " " + ex.Message);
                            System.Threading.Thread.Sleep(1000);
                        }
                    }
                }
                lblFtpUltimaAzione.Text = "Quantità file copiati: " + nomeFile.Count;

                lblDownload.Text = "Elimino i file in elenco";
                for (int i = 0; i < nomeFile.Count; i++)
                {
                    if (nomeFile[i].Substring(nomeFile[i].Length - 3).ToUpper() == "XML")
                    {
                        try
                        {
                            //cancella file
                            request = (FtpWebRequest)WebRequest.Create(ftpHost + "/" + lstFile.Items[i]);
                            request.Credentials = new NetworkCredential(ftpUser, ftpPassword);
                            this.Text = "Cancello " + lstFile.Items[i];
                            request.Method = WebRequestMethods.Ftp.DeleteFile;
                            response = (FtpWebResponse)request.GetResponse();
                            responseStream = response.GetResponseStream();
                            reader = new StreamReader(responseStream);
                            reader.Close();
                            response.Close();
                            this.Text = "Ho cancellato " + lstFile.Items[i];
                        }
                        catch (Exception ex)
                        {
                            this.Text = lstFile.Items[i] + " " + ex.Message;
                            System.Threading.Thread.Sleep(1000);
                        }
                    }
                }
            }
            catch { }

            lblFtpUltimaAzione.Text = "Quantità file copiati: " + nomeFile.Count;
            if (nomeFile.Count==0)
                lblFtpUltimaAzione.Text = "Attesa deposito file";
            lstFile.Items.Clear();
            lblDownload.Text = "Attesa";
            grpDownload.BackColor = Color.FromName("Control");
            timerDownload.Stop();
            timerDownload.Start();
        }

        public void FileUploadSFTP()
        {
            //var host = "whateverthehostis.com";
            //var port = 22;
            //var username = "username";
            //var password = "passw0rd";

            //// path for file you want to upload
            //var uploadFile = @"c:yourfilegoeshere.txt";

            //sftpclient client = new SmtpClient("sftp://00000031.fatturazione-elettronica-preprod.communicoincloud.com", 22);
            //client.Credentials=new NetworkCredential("user", "1079Dw54");
            //client.Port = 22;
            
            //client.Connect();
            //    if (client.IsConnected)
            //    {
            //        Debug.WriteLine("I'm connected to the client");

            //        using (var fileStream = new FileStream(uploadFile, FileMode.Open))
            //        {

            //            client.BufferSize = 4 * 1024; // bypass Payload error large files
            //            client.UploadFile(fileStream, Path.GetFileName(uploadFile));
            //        }
            //    }
            //    else
            //    {
            //        Debug.WriteLine("I couldn't connect");
            //    }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            sftpConnection();
            //lstFile.Items.Clear();
            //List<string> nomeFile = new List<string>();
            //string ftpServer = "sftp://00000031.fatturazione-elettronica-preprod.communicoincloud.com";

            //FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpServer);
            //request.Credentials = new NetworkCredential("user", "1079Dw54");
            //request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            //FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            //Stream responseStream = response.GetResponseStream();

            //StreamReader reader = new StreamReader(responseStream);
            //while (reader.EndOfStream == false)
            //{
            //    nomeFile.Add(reader.ReadLine().Substring(39));
            //}
            //reader.Close();
            //response.Close();
        }

        private void CompattaFile()
        {
            Application.DoEvents();
            grpUpload.BackColor = Color.LightSalmon;
            grpUpload.Refresh();
            //Ricerca file da compattare
            DirectoryInfo myDir = new DirectoryInfo(uplLocalFolder);
            foreach (FileInfo myFile in myDir.GetFiles())
            {
                try
                {
                    myFile.MoveTo(upLocalZipFolder + "\\" + myFile.Name);
                }
                catch { }
            }

            //Compattamento file
            myDir = new DirectoryInfo(upLocalZipFolder);
            if (myDir.GetFiles().Count() > 0)
            {
                try
                {
                    string nomeFile =  DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + ".zip";
                    ZipFile.CreateFromDirectory(upLocalZipFolder, uplLocalZipToSend + "\\" + nomeFile);
                    lblUltimoZip.Text = nomeFile;
                    foreach (FileInfo myFile in myDir.GetFiles())
                    {
                        try
                        {
                            myFile.Delete();
                        }
                        catch { }
                    }
                }
                catch (Exception ex ){ }
            }

            //Invio file compattato alla cartella SFTP
            //myDir = new DirectoryInfo(uplLocalZipToSend);
            //if (myDir.GetFiles().Count() > 0)
            //{
            //    try
            //    {
            //        foreach (FileInfo myFile in myDir.GetFiles())
            //        {
            //            try
            //            {
            //                myFile.MoveTo(uplLocalZipSended + "\\" + myFile.Name);
            //            }
            //            catch { }
            //        }
            //    }
            //    catch { }
            //}
            grpUpload.BackColor = Color.FromName("Control");
            timerUpload.Stop();
            timerUpload.Start();
        }
        private void txtCompatta_Click(object sender, EventArgs e)
        {
            CompattaFile();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            dwlFtpFolder = myIniConfig.GetValue("downloadOption", "FTPFolder");
            dwlLocalFolder = myIniConfig.GetValue("downloadOption", "LocalFolder");
            ftpHost = myIniConfig.GetValue("ftpcredentials", "host") + "/" + dwlFtpFolder;
            ftpUser = myIniConfig.GetValue("ftpcredentials", "user");
            ftpPassword = myIniConfig.GetValue("ftpcredentials", "password");
            txtFtpHost.Text = ftpHost;
            txtCartellaLocale.Text = dwlLocalFolder;

            uplLocalFolder = myIniConfig.GetValue("uploadOption", "LocalFolder");
            upLocalZipFolder = myIniConfig.GetValue("uploadOption", "LocalZipFolder");
            uplLocalZipToSend = myIniConfig.GetValue("uploadOption", "LocalZipToSend");
            uplLocalZipSended = myIniConfig.GetValue("uploadOption", "LocalZipSended");
            uplSftpFolder = myIniConfig.GetValue("uploadOption", "SFTPFolder");
            sftpHost = myIniConfig.GetValue("sftpcredentials", "host") + "/" + uplSftpFolder;
            sftpUser = myIniConfig.GetValue("sftpcredentials", "user");
            sftpPassword = myIniConfig.GetValue("sftpcredentials", "password");
            txtSftpHost.Text = sftpHost;
            txtZipFolder.Text = uplLocalZipToSend;
            

            myTrayIcon.Text="Arca Billing Sync";
            timerDownload.Interval = myIniConfig.GetInteger("downloadOption", "minutidiattesa") * 60000;
            timerUpload.Interval = myIniConfig.GetInteger("UploadOption", "minutidiattesa") * 60000;
            timerDownload.Tick += new EventHandler(Download);
            timerUpload.Tick += new EventHandler(Upload);
            timerDownload.Start();
            timerUpload.Start();
            WindowState = FormWindowState.Minimized;
        }
     
        private void Download(object muObject, EventArgs myEventArgs)
        {
            SpostaFileDaFTP();
            //DirectoryInfo myDir = new DirectoryInfo(dwlFtpFolder);
            //foreach (FileInfo myFile in myDir.GetFiles())
            //{
            //    try
            //    {
            //        if (myFile.Extension.ToString()==".txt")
            //            myFile.MoveTo(dwlLocalFolder + "\\" + myFile.Name); 
            //    }
            //    catch
            //    { }
            //}
            //counterDownload += 1;
            //this.Text = "Download: " + counterDownload.ToString() + " - Upload: " + counterUpload.ToString();
        }
        private void Upload(object muObject, EventArgs myEventArgs)
        {
            CompattaFile();
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            if (WindowState==FormWindowState.Minimized)
            {
                ShowIcon = false;
                myTrayIcon.Visible = true;
                ShowInTaskbar = false;
                //myTrayIcon.BalloonTipText = "CIAO";
                //myTrayIcon.BalloonTipTitle = "TITOLO";
                //myTrayIcon.ShowBalloonTip(1000);

            }
        }

        private void myTrayIcon_DoubleClick(object sender, EventArgs e)
        {
            ShowInTaskbar = true;
            myTrayIcon.Visible = false;
            WindowState = FormWindowState.Normal;
        }
    }
}
